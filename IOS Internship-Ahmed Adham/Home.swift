//
//  Home.swift
//  IOS Internship-Ahmed Adham
//
//  Created by Ahmed adham on 10/14/18.
//  Copyright © 2018 Adham. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON

class Home: UIViewController , UITableViewDelegate , UITableViewDataSource{

    
    @IBOutlet weak var tableCountry: UITableView!

    
    var countryList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let api = "https://restcountries.eu/rest/v2/all?fields=name"
        let url = URL(string: api)
        
        loadCountryList(url: url!)
        
        tableCountry.delegate = self
        tableCountry.dataSource = self
        
    
    }

    @IBAction func backPreesed(_ sender: Any) {
        do {
          try  Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        }
        catch{
           self.showAlert()
        }
        
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "oops", message: "you have not loged out yet", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }

    func loadCountryList(url : URL){
       
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { (response) in
            if response.result.isSuccess {
                print("Success!")
                let jsonData : JSON = JSON(response.result.value!)
                self.parseData(json: jsonData)
            }
            else{
                print("Error : \(String(describing: response.result.error))")
            }
            
        }
    }
    
    func parseData(json : JSON){
        for item in json.arrayValue {
           countryList.append(item["name"].stringValue)
        }
        print(countryList)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableCountry.dequeueReusableCell(withIdentifier: "Cell" , for: indexPath) 
        cell.textLabel?.text = countryList[indexPath.row]
        return cell
    }
    
    
  /*  func loadCountryList(URL: URL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if (error == nil) {
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode
                print("Success: \(statusCode)")
                
                // This is your file-variable:
                if let data = data {
                    
                    let nsData = NSData(data: data)
                    do {
                        let Json =  try JSONSerialization.jsonObject(with: nsData as Data, options: .mutableContainers) as? [String : Any]
                    }
                    catch{
                        print(error)
                    }

                
                }
            }
            else {
                // Failure
                print("Failure: %@", error!.localizedDescription);
            }
        }
        
        
        task.resume()
    }
    */

}
