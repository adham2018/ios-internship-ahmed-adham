//
//  loginVC.swift
//  IOS Internship-Ahmed Adham
//  ahmedadham791@gmail.com
//  Created by Ahmed adham on 10/14/18.
//  Copyright © 2018 Adham. All rights reserved.
//

import UIKit
import Firebase
class LoginVC : UIViewController {
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func logInPressed(_ sender: Any) {
        if emailText.text != nil && passwordText.text != nil {
        Auth.auth().signIn(withEmail: emailText.text!, password: passwordText.text!, completion: { (user, error) in
            if error != nil {
                self.showAlert()
                print(error!)
            }
            else{
                self.gotoHome()
                print(user!)
            }
            
            })

        }
    }
    
    func gotoHome(){
        performSegue(withIdentifier: "logInToHome", sender: nil)
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Error", message: "you must register before login", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }

}

extension LoginVC : UITextFieldDelegate {

}

