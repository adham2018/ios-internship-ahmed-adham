//
//  RegistrationVCViewController.swift
//  IOS Internship-Ahmed Adham
//
//  Created by Ahmed adham on 10/14/18.
//  Copyright © 2018 Adham. All rights reserved.
//

import UIKit
import Firebase

class RegistrationVC : UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func registerPressed(_ sender: Any) {
        if emailField.text != nil && passwordField.text != nil && confirmPasswordField.text != nil {
            if passwordField.text! == confirmPasswordField.text!{
                Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!, completion: { (user, error) in
                    if error != nil {
                        self.showAlert()
                        print("Error : \(error!)")
                    }
                    else {
                        self.goToHome()
                    }
                })
            }
        }
            }

    func goToHome(){
        performSegue(withIdentifier: "registerToHome", sender: nil)
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Error", message: "you have not registered yet", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
